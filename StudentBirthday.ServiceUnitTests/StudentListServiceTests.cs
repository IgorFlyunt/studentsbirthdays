﻿using System;
using System.Linq;
using Moq;
using NUnit.Framework;
using StudentBirthday.Data;

namespace StudentBirthday.Service.UnitTests
{
    [TestFixture]
    public class StudentListServiceTests
    {
        [Test]
        public void GetBirthdayInTheCurrentMonthTest()
        {
            var data = new[]
            {
                new Student {Id = 1, Birthday = DateTime.Now},
                new Student {Id = 2, Birthday = DateTime.Now.AddMonths(1)}
            }.AsQueryable();

            var mock = new Mock<IDbContext>();
            mock.Setup(x => x.Set<Student>()).Returns(data);
            var context = new StudentListService(mock.Object);

            var currentMonth = context.GetBirthdayInTheCurrentMonth().ToList();

            Assert.IsTrue(currentMonth.Any());
            Assert.AreEqual(currentMonth.Count(), 1);
        }

        [Test]
        public void GetBirthdayInTheCurrentMonthAndSpecificClassTest()
        {
            var testId = 452;
            var data = new[]
            {
                new Student {Id = testId, Birthday = DateTime.Now, ClassesId = 1},
                new Student {Id = 2, Birthday = DateTime.Now.AddMonths(1), ClassesId = 2}
            }.AsQueryable();
            var mock = new Mock<IDbContext>();
            mock.Setup(x => x.Set<Student>()).Returns(data);
            var context = new StudentListService(mock.Object);

            var currentMonth = context.GetBirthdayInTheCurrentMonthAndSpecificClass(0).ToList();
            Assert.AreEqual(currentMonth.Count, 0);
            currentMonth = context.GetBirthdayInTheCurrentMonthAndSpecificClass(1).ToList();
            Assert.AreEqual(currentMonth.Count(), 1);
            Assert.AreEqual(testId, currentMonth.First().Id);
        }

        [Test]
        public void GetListMonthAndStudentsWhoHaveBirthdayTest()
        {
            var data = new[]
            {
                new Student {Id = 1, Birthday = new DateTime(1990, 1, 1)},
                new Student {Id = 2, Birthday = new DateTime(1990, 1, 1)},
                new Student {Id = 3, Birthday = new DateTime(1990, 2, 1)},
                new Student {Id = 4, Birthday = new DateTime(1990, 3, 1)}
            }.AsQueryable();

            var mock = new Mock<IDbContext>();
            mock.Setup(x => x.Set<Student>()).Returns(data);
            var context = new StudentListService(mock.Object);

            var currentMonth = context.GetListMonthAndStudentsWhoHaveBirthday().ToList();
            var monthIndex = 1;
            foreach (var (key, value) in currentMonth)
            {
                Assert.IsTrue(value.Any());

                switch (monthIndex)
                {
                    case 1:
                        Assert.IsTrue(value.Count == 2 && key == "January");
                        break;
                    case 2:
                        Assert.IsTrue(value.Count == 1 && key == "February");
                        break;
                    case 3:
                        Assert.IsTrue(value.Count == 1 && key == "March" &&
                                      value.First().Id == 4);
                        break;
                    case 6:
                        Assert.Fail("Doesn't have a student in the current month");
                        break;
                }

                monthIndex++;
            }
        }

        [Test]
        public void GetSortedOldestStudentsTest()
        {
            var data = new[]
            {
                new Student {Id = 1, Birthday = new DateTime(1990, 1, 1)},
                new Student {Id = 2, Birthday = new DateTime(1992, 1, 1)},
                new Student {Id = 3, Birthday = new DateTime(1994, 2, 1)},
                new Student {Id = 4, Birthday = new DateTime(1996, 3, 1)}
            }.AsQueryable();

            var mock = new Mock<IDbContext>();
            mock.Setup(x => x.Set<Student>()).Returns(data);
            var context = new StudentListService(mock.Object);

            var testCount = 4;
            var currentMonth = context.GetSortedStudents(testCount, true).ToList();
            Assert.AreEqual(currentMonth.Count, testCount);
            Assert.AreEqual(1990, currentMonth.First().Birthday.Year);
        }

        [Test]
        public void GetSortedYoungestStudentsTest()
        {
            var data = new[]
            {
                new Student {Id = 1, Birthday = new DateTime(1990, 1, 1)},
                new Student {Id = 2, Birthday = new DateTime(1992, 1, 1)},
                new Student {Id = 3, Birthday = new DateTime(1994, 2, 1)},
                new Student {Id = 4, Birthday = new DateTime(1996, 3, 1)}
            }.AsQueryable();

            var mock = new Mock<IDbContext>();
            mock.Setup(x => x.Set<Student>()).Returns(data);
            var context = new StudentListService(mock.Object);

            var testCount = 3;
            var currentMonth = context.GetSortedStudents(testCount, false).ToList();
            Assert.AreEqual(currentMonth.Count, testCount);
            var testOrderData = data.OrderByDescending(x => x.Birthday);
            Assert.AreEqual(testOrderData.First().Birthday.Month, currentMonth.First().Birthday.Month);
        }
    }
}