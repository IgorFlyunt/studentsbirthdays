﻿using StudentBirthday.Data;
using StudentBirthday.Service;

namespace StudentBirthday.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var context = new StudentBirthdayContext();
            var getListOfStudents = new StudentListService(context);

            var currentMonth = getListOfStudents.GetBirthdayInTheCurrentMonth();
            foreach (var student in currentMonth)
                System.Console.WriteLine(
                    $@"id : {student.Id}, Full Name : {student.FirstName}  {student.LastName},
                            Date Birth : {student.Birthday.ToShortDateString()}");

            var currentMonthAndClass = getListOfStudents.GetBirthdayInTheCurrentMonthAndSpecificClass(1);
            foreach (var student in currentMonthAndClass)
                System.Console.WriteLine(
                    $@"id : {student.Id}, Full Name : {student.FirstName}  {student.LastName},
                            Date Birth : {student.Birthday.ToShortDateString()}");

            var listMonthAndStudents = getListOfStudents.GetListMonthAndStudentsWhoHaveBirthday();
            foreach (var month in listMonthAndStudents)
            {
                System.Console.WriteLine(month.Key);
                foreach (var student in month.Value)
                    System.Console.WriteLine(
                        $@"id : {student.Id}, Full Name : {student.FirstName}  {student.LastName},
                                Date Birth : {student.Birthday.ToShortDateString()}");
            }

            var sortedStudents = getListOfStudents.GetSortedStudents(5, true);
            foreach (var student in sortedStudents)
                System.Console.WriteLine(
                    $@"id : {student.Id}, Full Name : {student.FirstName}  {student.LastName}, 
                            Date Birth : {student.Birthday.ToShortDateString()}");
        }
    }
}