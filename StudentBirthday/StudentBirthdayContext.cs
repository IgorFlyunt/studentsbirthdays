﻿using System.Data.Entity;
using System.Linq;
using StudentBirthday.Data.EntityConfigurations;

namespace StudentBirthday.Data
{
    public class StudentBirthdayContext : DbContext, IDbContext
    {
        public StudentBirthdayContext() : base(
            "data source=SURFACE; initial catalog=StudentBirthday; integrated security=SSPI")
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Class> Classes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguration());
            modelBuilder.Configurations.Add(new ClassConfiguration());
        }

        IQueryable<T> IDbContext.Set<T>()
        {
            return base.Set<T>();
        }
    }
}