﻿using System.Linq;

namespace StudentBirthday.Data
{
    public interface IDbContext
    {
        IQueryable<T> Set<T>() where T : class;
    }
}
