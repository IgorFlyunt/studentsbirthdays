﻿using System.Collections.Generic;

namespace StudentBirthday.Data
{
    public class Class
    {
        public Class()
        {
            Students = new HashSet<Student>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}