﻿using System.Collections.Generic;

namespace StudentBirthday.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<StudentBirthday.Data.StudentBirthdayContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StudentBirthday.Data.StudentBirthdayContext context)
        {
            #region Add Students

            var students = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    FirstName = "Devante",
                    LastName = "Wilcox",
                    Birthday = new DateTime(1994, 04, 10),
                    ClassesId = 1
                },
                new Student
                {
                    Id = 2,
                    FirstName = "Claudia",
                    LastName = "Andrew",
                    Birthday = new DateTime(1998, 12, 25),
                    ClassesId = 1
                },
                new Student
                {
                    Id = 3,
                    FirstName = "Eleni",
                    LastName = "Farrow",
                    Birthday = new DateTime(1997, 02, 11),
                    ClassesId = 1
                },
                new Student
                {
                    Id = 4,
                    FirstName = "Saffron",
                    LastName = "Richard",
                    Birthday = new DateTime(1991, 11, 12),
                    ClassesId = 1
                },
                new Student
                {
                    Id = 5,
                    FirstName = "Maira",
                    LastName = "Greenwood",
                    Birthday = new DateTime(1993, 09, 05),
                    ClassesId = 2
                },
                new Student
                {
                    Id = 6,
                    FirstName = "Yanis",
                    LastName = "Joseph",
                    Birthday = new DateTime(1995, 04, 19),
                    ClassesId = 2
                },
                new Student
                {
                    Id = 7,
                    FirstName = "Yvie",
                    LastName = "Hughes",
                    Birthday = new DateTime(2002, 08, 24),
                    ClassesId = 2
                },
                new Student
                {
                    Id = 8,
                    FirstName = "Andrea",
                    LastName = "Lutz",
                    Birthday = new DateTime(1996, 10, 13),
                    ClassesId = 2
                },
                new Student
                {
                    Id = 9,
                    FirstName = "Malachy",
                    LastName = "Hibbert",
                    Birthday = new DateTime(1992, 05, 04),
                    ClassesId = 3
                },
                new Student
                {
                    Id = 10,
                    FirstName = "Dominika",
                    LastName = "Burke",
                    Birthday = new DateTime(1999, 08, 14),
                    ClassesId = 3
                },
                new Student
                {
                    Id = 11,
                    FirstName = "Ahsan",
                    LastName = "Simon",
                    Birthday = new DateTime(1994, 09, 22),
                    ClassesId = 3
                },
                new Student
                {
                    Id = 12,
                    FirstName = "Aditya",
                    LastName = "Day",
                    Birthday = new DateTime(1995, 12, 08),
                    ClassesId = 3
                },
                new Student
                {
                    Id = 13,
                    FirstName = "Rose",
                    LastName = "Crane",
                    Birthday = new DateTime(2000, 11, 05),
                    ClassesId = 4
                },
                new Student
                {
                    Id = 14,
                    FirstName = "Glyn",
                    LastName = "Leblanc",
                    Birthday = new DateTime(1999, 09, 14),
                    ClassesId = 4
                },
                new Student
                {
                    Id = 15,
                    FirstName = "Ruth",
                    LastName = "Davie",
                    Birthday = new DateTime(2001, 02, 26),
                    ClassesId = 4
                },
                new Student
                {
                    Id = 16,
                    FirstName = "Leia",
                    LastName = "Hopkins",
                    Birthday = new DateTime(1996, 07, 05),
                    ClassesId = 4
                },
                new Student
                {
                    Id = 17,
                    FirstName = "Khalil",
                    LastName = "Guest",
                    Birthday = new DateTime(1997, 01, 17),
                    ClassesId = 5
                },
                new Student
                {
                    Id = 18,
                    FirstName = "Kye",
                    LastName = "Houghton",
                    Birthday = new DateTime(1998, 03, 11),
                    ClassesId = 5
                },
                new Student
                {
                    Id = 19,
                    FirstName = "Holli",
                    LastName = "Morales",
                    Birthday = new DateTime(2000, 10, 01),
                    ClassesId = 5
                },
                new Student
                {
                    Id = 20,
                    FirstName = "Steven",
                    LastName = "Ponce",
                    Birthday = new DateTime(2001, 04, 26),
                    ClassesId = 5
                }
            };
            foreach (var student in students)
                context.Students.AddOrUpdate(s => s.Id, student);

            #endregion

            #region Add Classes

            var classes = new List<Class>
            {
                new Class()
                {
                    Id = 1,
                    Name = "App development"
                },
                new Class()
                {
                    Id = 2,
                    Name = "Audio production"
                },
                new Class()
                {
                    Id = 3,
                    Name = "Web design"
                },
                new Class()
                {
                    Id = 4,
                    Name = "Video game development"
                },
                new Class()
                {
                    Id = 5,
                    Name = "Film production"
                }
            };
            foreach (var itemClass in classes)
                context.Classes.AddOrUpdate(c => c.Id, itemClass);
            #endregion

        }
    }
}
