﻿using System.Data.Entity.ModelConfiguration;

namespace StudentBirthday.Data.EntityConfigurations
{
    public class ClassConfiguration : EntityTypeConfiguration<Class>
    {
        public ClassConfiguration()
        {
            Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}