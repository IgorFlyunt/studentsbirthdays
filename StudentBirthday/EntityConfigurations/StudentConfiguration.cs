﻿using System.Data.Entity.ModelConfiguration;
namespace StudentBirthday.Data.EntityConfigurations
{
    public class StudentConfiguration : EntityTypeConfiguration<Student>
    {
        public StudentConfiguration()
        {
            Property(s => s.FirstName)
                .IsRequired()
                .HasMaxLength(50);
            Property(s => s.LastName)
                .IsRequired()
                .HasMaxLength(50);
            HasRequired(s => s.Classes)
                .WithMany(c => c.Students)
                .HasForeignKey(s => s.ClassesId);
        }
    }
}