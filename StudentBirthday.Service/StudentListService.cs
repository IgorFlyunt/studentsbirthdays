﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using StudentBirthday.Data;

namespace StudentBirthday.Service
{
    public class StudentListService
    {
        private readonly IDbContext _context;

        public StudentListService(IDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Student> GetBirthdayInTheCurrentMonth()
        {
            return _context.Set<Student>()
                .Where(s => s.Birthday.Month == DateTime.Now.Month)
                .ToList();
        }

        public IEnumerable<Student> GetBirthdayInTheCurrentMonthAndSpecificClass(int classId)
        {
            return _context.Set<Student>()
                .Where(s => s.Birthday.Month == DateTime.Now.Month && s.ClassesId == classId)
                .ToList();
        }

        public Dictionary<string, List<Student>> GetListMonthAndStudentsWhoHaveBirthday()
        {
            var dictionary = new Dictionary<string, List<Student>>();
            var grouping = _context.Set<Student>()
                .GroupBy(s => s.Birthday.Month);
            foreach (var student in grouping)
            {
                var monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(student.Key);
                dictionary.Add(monthName, student.ToList());
            }

            return dictionary;
        }

        public IEnumerable<Student> GetSortedStudents(int count, bool sortAscending)
        {
            return sortAscending
                ? _context.Set<Student>()
                    .OrderBy(s => s.Birthday)
                    .Take(count)
                    .ToList()
                : _context.Set<Student>()
                    .OrderByDescending(s => s.Birthday)
                    .Take(count)
                    .ToList();
        }
    }
}